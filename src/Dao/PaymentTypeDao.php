<?php

namespace TimKipp\Intersect\Orders\Dao;

use TimKipp\Intersect\Database\Dao\AbstractDao;
use TimKipp\Intersect\Orders\Domain\PaymentType;

/**
 * Class PaymentTypeDao
 * @package TimKipp\Intersect\Order\Dao
 */
class PaymentTypeDao extends AbstractDao {

    /**
     * @return string
     */
    public function getDomainClass()
    {
        return PaymentType::class;
    }

}