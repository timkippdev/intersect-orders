<?php

namespace TimKipp\Intersect\Orders\Dao;

use TimKipp\Intersect\Database\Dao\AbstractDao;
use TimKipp\Intersect\Orders\Domain\Payment;
use TimKipp\Intersect\Orders\Validation\PaymentValidator;

/**
 * Class PaymentDao
 * @package TimKipp\Intersect\Order\Dao
 */
class PaymentDao extends AbstractDao {

    /**
     * @return string
     */
    public function getDomainClass()
    {
        return Payment::class;
    }

    /**
     * @return PaymentValidator
     */
    public function getValidator()
    {
        return new PaymentValidator();
    }

}