<?php

namespace TimKipp\Intersect\Orders\Dao;

use TimKipp\Intersect\Database\Dao\AbstractDao;
use TimKipp\Intersect\Database\Query\SelectQuery;
use TimKipp\Intersect\Orders\Domain\Order;
use TimKipp\Intersect\Orders\Validation\OrderValidator;

/**
 * Class OrderDao
 * @package TimKipp\Intersect\Order\Dao
 */
class OrderDao extends AbstractDao {

    /**
     * @return string
     */
    public function getDomainClass()
    {
        return Order::class;
    }

    /**
     * @return OrderValidator
     */
    public function getValidator()
    {
        return new OrderValidator();
    }

    /**
     * @param $accountId
     * @return array
     */
    public function getOrdersForAccountId($accountId)
    {
        $query = SelectQuery::table($this->getTable())->where('account_id', (int) $accountId)->orderBy('date_created');
        return $this->getAdapter()->run($query)->getRecords();
    }

}