<?php

class Migration_1_0_0_Orders extends \TimKipp\Intersect\Migration\AbstractMigration {

    public function getVersion()
    {
        return '1.0.0-intersect-orders';
    }

    public function migrateUp()
    {
        $this->getAdapter()->run(\TimKipp\Intersect\Database\Query\RawQuery::init("
            CREATE TABLE IF NOT EXISTS `order_payment_type` (
              `payment_type_id` INT (11) NOT NULL AUTO_INCREMENT,
              `name` VARCHAR (50) NOT NULL,
              `date_created` DATETIME NOT NULL,
              PRIMARY KEY (`payment_type_id`),
              UNIQUE KEY unique_idx_name (`name`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8;
        "));

        $this->getAdapter()->run(\TimKipp\Intersect\Database\Query\InsertQuery::table('order_payment_type')->values(array('name' => 'Cash', 'date_created' => 'NOW()')));
        $this->getAdapter()->run(\TimKipp\Intersect\Database\Query\InsertQuery::table('order_payment_type')->values(array('name' => 'Stripe', 'date_created' => 'NOW()')));

        $this->getAdapter()->run(\TimKipp\Intersect\Database\Query\RawQuery::init("
            CREATE TABLE IF NOT EXISTS `order_payment` (
              `payment_id` INT (11) NOT NULL AUTO_INCREMENT,
              `payment_type_id` INT (11) NOT NULL,
              `amount` DECIMAL (7, 2) NOT NULL,
              `confirmation_number` VARCHAR (100) NOT NULL,
              `external_confirmation_number` VARCHAR (255),
              `date_created` DATETIME NOT NULL,
              PRIMARY KEY (`payment_id`),
              INDEX `idx_confirmation_number` (`confirmation_number`),
              INDEX `idx_payment_type_id` (`payment_type_id`),
              UNIQUE KEY unique_idx_confirmation_number (`confirmation_number`),
              FOREIGN KEY (`payment_type_id`) REFERENCES order_payment_type (`payment_type_id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8;
        "));

        $this->getAdapter()->run(\TimKipp\Intersect\Database\Query\RawQuery::init("
            CREATE TABLE IF NOT EXISTS `order` (
              `order_id` INT (11) NOT NULL AUTO_INCREMENT,
              `account_id` INT (11),
              `order_number` VARCHAR (255) NOT NULL,
              `type` VARCHAR (100),
              `details` TEXT NOT NULL,
              `total` DECIMAL (7, 2) NOT NULL,
              `status` TINYINT (2) NOT NULL,
              `payment_id` INT (11),
              `date_created` DATETIME NOT NULL,
              `date_updated` DATETIME,
              `date_payment_received` DATETIME,
              `date_fulfilled` DATETIME,
              `date_cancelled` DATETIME,
              PRIMARY KEY (`order_id`),
              INDEX `idx_account_id` (`account_id`),
              INDEX `idx_type` (`type`),
              UNIQUE KEY unique_idx_order_number (`order_number`),
              FOREIGN KEY (`account_id`) REFERENCES account (`account_id`),
              FOREIGN KEY (`payment_id`) REFERENCES order_payment (`payment_id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8;
        "));
    }

    public function migrateDown()
    {
        $this->getAdapter()->dropTable("order");
        $this->getAdapter()->dropTable("order_payment");
        $this->getAdapter()->dropTable("order_payment_type");
    }

}