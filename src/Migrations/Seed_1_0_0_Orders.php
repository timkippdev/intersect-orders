<?php

class Seed_1_0_0_Orders extends \TimKipp\Intersect\Migration\AbstractMigrationSeed {

    private $accountService;
    private $orderService;

    public function __construct(\TimKipp\Intersect\Database\Adapters\AdapterInterface $adapter)
    {
        parent::__construct($adapter);

        $this->accountService = new \TimKipp\Intersect\Services\AccountService($adapter);
        $this->orderService = new \TimKipp\Intersect\Orders\Services\OrderService($adapter);
    }

    public function getVersion()
    {
        return '1.0.0-intersect-orders';
    }

    public function migrateUp()
    {
        $seedAccount = $this->createSeedAccount();

        $this->createSeedOrder($seedAccount->getAccountId());
        $this->createSeedOrderWithPaymentType($seedAccount->getAccountId(), \TimKipp\Intersect\Orders\Domain\PaymentType::CASH);
    }

    /**
     * @return \TimKipp\Intersect\Domain\Account
     */
    private function createSeedAccount()
    {
        $account = new \TimKipp\Intersect\Domain\Account();
        $account->setEmail('orders@intersect.com');
        $account->setPassword('password');
        $account->setStatus(\TimKipp\Intersect\Types\AccountStatusType::ACTIVE);
        $account->setRole(2);
        $account->setFirstName('Orders');
        $account->setLastName('Account');
        $account->setRegistrationSource('seed-data');

        return $this->accountService->create($account);
    }

    private function createSeedOrder($accountId)
    {
        $order = new \TimKipp\Intersect\Orders\Domain\Order();
        $order->setAccountId($accountId);
        $order->setType(\TimKipp\Intersect\Orders\OrderType::ONLINE);

        $shippingAddress = new \TimKipp\Intersect\Orders\Domain\ShippingAddress();
        $shippingAddress->setStreet('123 Jamie Road');
        $shippingAddress->setCity('Douglas');
        $shippingAddress->setState('WI');
        $shippingAddress->setZipCode(12345);

        $billingAddress = new \TimKipp\Intersect\Orders\Domain\BillingAddress();
        $billingAddress->setStreet('123 Rose Lane');
        $billingAddress->setCity('Douglas');
        $billingAddress->setState('WI');
        $billingAddress->setZipCode(12345);

        $orderDetails = new \TimKipp\Intersect\Orders\Domain\OrderDetails();
        $orderDetails->setSubtotal(19.99);
        $orderDetails->setShipping(3.45);
        $orderDetails->setTax(2.99);
        $orderDetails->setDiscount(1.99);
        $orderDetails->setShippingAddress($shippingAddress);
        $orderDetails->setBillingAddress($billingAddress);

        $order->setDetails($orderDetails);

        return $this->orderService->create($order);
    }

    private function createSeedOrderWithPaymentType($accountId, $paymentType)
    {
        $order = $this->createSeedOrder($accountId);

        $payment = new \TimKipp\Intersect\Orders\Domain\Payment();
        $payment->setPaymentTypeId($paymentType);
        $payment->setAmount(12.34);
        $payment->setExternalConfirmationNumber('external-seed-data');

        $this->orderService->createWithPayment($order, $payment);
    }

}