<?php

namespace TimKipp\Intersect\Orders\Event;

use TimKipp\Intersect\Event\Event;
use TimKipp\Intersect\Orders\Domain\Order;

/**
 * Class OrderCreatedEvent
 * @package TimKipp\Intersect\Event
 */
class OrderCreatedEvent extends Event {

    const ORDER_CREATED = 'ORDER_CREATED';

    private $createdOrder;

    /**
     * OrderCreatedEvent constructor.
     * @param Order $createdOrder
     */
    public function __construct(Order $createdOrder)
    {
        $this->createdOrder = $createdOrder;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::ORDER_CREATED;
    }

    /**
     * @return Order
     */
    public function getCreatedOrder()
    {
        return $this->createdOrder;
    }

}