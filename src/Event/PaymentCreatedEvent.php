<?php

namespace TimKipp\Intersect\Orders\Event;

use TimKipp\Intersect\Event\Event;
use TimKipp\Intersect\Orders\Domain\Order;
use TimKipp\Intersect\Orders\Domain\Payment;

/**
 * Class PaymentCreatedEvent
 * @package TimKipp\Intersect\Event
 */
class PaymentCreatedEvent extends Event {

    const PAYMENT_CREATED = 'PAYMENT_CREATED';

    private $createdPayment;
    private $order;

    /**
     * PaymentCreatedEvent constructor.
     * @param Payment $createdPayment
     * @param Order $order
     */
    public function __construct(Payment $createdPayment, Order $order = null)
    {
        $this->createdPayment = $createdPayment;
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::PAYMENT_CREATED;
    }

    /**
     * @return Payment
     */
    public function getCreatedPayment()
    {
        return $this->createdPayment;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

}