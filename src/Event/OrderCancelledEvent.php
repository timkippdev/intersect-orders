<?php

namespace TimKipp\Intersect\Orders\Event;

use TimKipp\Intersect\Event\Event;
use TimKipp\Intersect\Orders\Domain\Order;

/**
 * Class OrderCancelledEvent
 * @package TimKipp\Intersect\Event
 */
class OrderCancelledEvent extends Event {

    const ORDER_CANCELLED = 'ORDER_CANCELLED';

    private $cancelledOrder;

    /**
     * OrderCancelledEvent constructor.
     * @param Order $cancelledOrder
     */
    public function __construct(Order $cancelledOrder)
    {
        $this->cancelledOrder = $cancelledOrder;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::ORDER_CANCELLED;
    }

    /**
     * @return Order
     */
    public function getCancelledOrder()
    {
        return $this->cancelledOrder;
    }

}