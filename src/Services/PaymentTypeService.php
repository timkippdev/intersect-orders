<?php

namespace TimKipp\Intersect\Orders\Services;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;
use TimKipp\Intersect\Orders\Dao\PaymentTypeDao;
use TimKipp\Intersect\Services\AbstractService;

/**
 * Class PaymentTypeService
 * @package TimKipp\Intersect\Order\Services
 */
class PaymentTypeService extends AbstractService {

    /**
     * PaymentTypeService constructor.
     * @param AdapterInterface $databaseAdapter
     */
    public function __construct(AdapterInterface $databaseAdapter)
    {
        parent::__construct($databaseAdapter);

        $this->setDao(new PaymentTypeDao($databaseAdapter));
    }

}