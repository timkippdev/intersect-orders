<?php

namespace TimKipp\Intersect\Orders\Services;

use Stripe\Charge;
use Stripe\Stripe;
use TimKipp\Intersect\Database\Adapters\AdapterInterface;

class StripePaymentService {

    private static $STRIPE_SCRIPT_SRC = 'https://checkout.stripe.com/checkout.js';

    private $currencyType;
    private $orderService;
    private $publishableKey;
    private $secretKey;

    public function __construct(AdapterInterface $databaseAdapter, $publishableKey, $secretKey, $currencyType = 'usd')
    {
        $this->orderService = new OrderService($databaseAdapter);
        $this->publishableKey = $publishableKey;
        $this->secretKey = $secretKey;
        $this->currencyType = $currencyType;

        Stripe::setApiKey($secretKey);
    }

    /**
     * @param $paymentToken
     * @return Payment
     * @throws StripePaymentException
     */
    public function processPayment($paymentToken, $amountInCents, $paymentDescription)
    {
        try {
            $charge = Charge::create(array(
                'source' => $paymentToken,
                'amount' => $amountInCents,
                'description' => $paymentDescription,
                'currency' => $this->currencyType,
            ));

            $payment = new Payment();
            $payment->setPaymentTypeId(PaymentType::STRIPE);
            $payment->setExternalConfirmationNumber($charge->id);
            $payment->setAmount(number_format(($charge->amount / 100),2));

            return $payment;
        } catch (\Exception $e) {
            throw new StripePaymentException($e->getMessage(), $e->getCode());
        }
    }

    public function getCheckoutButtonScript(CheckoutButtonScriptOptions $options)
    {
        return '<script src="' . self::$STRIPE_SCRIPT_SRC . '" 
            class="' . $options->getClass() . '"
            data-key="' . $this->publishableKey . '"
            data-email="' . $options->getEmail() . '"
            data-label="' . $options->getLabel() . '"
            data-description="' . $options->getDescription() . '"
            data-amount="' . $options->getAmount() . '"    
            data-zip-code="' . ($options->isRequireZipCode() ? 'true' : 'false') . '">
        </script>';
    }

}