<?php

namespace TimKipp\Intersect\Orders\Services;

use TimKipp\Intersect\Database\Adapters\AdapterInterface;
use TimKipp\Intersect\Domain\AbstractDomain;
use TimKipp\Intersect\Orders\Dao\OrderDao;
use TimKipp\Intersect\Orders\Dao\PaymentDao;
use TimKipp\Intersect\Orders\Domain\Order;
use TimKipp\Intersect\Orders\Domain\Payment;
use TimKipp\Intersect\Orders\Event\OrderCancelledEvent;
use TimKipp\Intersect\Orders\Event\OrderCreatedEvent;
use TimKipp\Intersect\Orders\Event\PaymentCreatedEvent;
use TimKipp\Intersect\Orders\OrderStatusType;
use TimKipp\Intersect\Orders\Validation\OrderValidator;
use TimKipp\Intersect\Services\AbstractService;

/**
 * Class OrderService
 * @package TimKipp\Intersect\Order\Services
 *
 * @method OrderDao getDao
 * @method OrderValidator getValidator
 */
class OrderService extends AbstractService {

    /** @var PaymentDao $paymentDao */
    private $paymentDao;

    /**
     * OrderService constructor.
     * @param AdapterInterface $databaseAdapter
     */
    public function __construct(AdapterInterface $databaseAdapter)
    {
        parent::__construct($databaseAdapter);

        $this->setDao(new OrderDao($databaseAdapter));
        $this->paymentDao = new PaymentDao($databaseAdapter);
    }

    /**
     * @param PaymentDao $paymentDao
     */
    public function setPaymentDao(PaymentDao $paymentDao)
    {
        $this->paymentDao = $paymentDao;
    }

    /**
     * @param Order|AbstractDomain $order
     * @return Order|null
     */
    public function create(AbstractDomain $order)
    {
        /** @var Order $order */

        $order->setStatus(OrderStatusType::PENDING);
        $order->setOrderNumber($this->generateUniqueOrderNumber($order));
        $order->setTotal($order->getRawDetails()->getTotal());

        $createdOrder = parent::create($order);

        if (!is_null($createdOrder))
        {
            $this->getEventDispatcher()->dispatch(new OrderCreatedEvent($createdOrder), 'Order created successfully.');
        }

        return $createdOrder;
    }

    /**
     * @param Order $order
     * @param Payment $payment
     * @return null|Order
     * @throws \Exception
     */
    public function createWithPayment(Order $order, Payment $payment)
    {
        $createdOrder = $this->create($order);

        if (!is_null($createdOrder))
        {
            $payment = $this->makePaymentOnOrder($payment, $createdOrder);
            if (!is_null($payment))
            {
                $createdOrder->setPayment($payment);
            }
        }

        return $createdOrder;
    }

    /**
     * @param $orderNumber
     * @return Order|null
     */
    public function getOrderByOrderNumber($orderNumber)
    {
        return $this->getDao()->getAllBy('order_number', $orderNumber, 1);
    }

    /**
     * @param $accountId
     * @param bool $includePayments
     * @return array
     */
    public function getOrdersForAccountId($accountId, $includePayments = false)
    {
        $orders = $this->getDao()->getOrdersForAccountId($accountId);

        if ($includePayments)
        {
            /** @var Order $order */
            foreach ($orders as $order)
            {
                if (!is_null($order->getPaymentId()))
                {
                    $payment = $this->getPaymentById($order->getPaymentId());
                    if (!is_null($payment))
                    {
                        $order->setPayment($payment);
                    }
                }
            }
        }

        return $orders;
    }

    /**
     * @param Order $order
     * @return Order
     * @throws \TimKipp\Intersect\Validation\ValidationException
     */
    public function cancelOrder(Order $order)
    {
        $this->getValidator()->validateCancel($order);

        $order->setStatus(OrderStatusType::CANCELLED);
        $order->setDateCancelled($this->getMySQLNow());

        /** @var Order $cancelledOrder */
        $cancelledOrder = $this->update($order, $order->getOrderId());

        if (!is_null($cancelledOrder) && $cancelledOrder->getStatus() == OrderStatusType::CANCELLED)
        {
            $this->getEventDispatcher()->dispatch(new OrderCancelledEvent($cancelledOrder), 'Order cancelled successfully.');
        }

        return $cancelledOrder;
    }

    /**
     * @param Payment $payment
     * @param Order $order
     * @return Payment
     * @throws \Exception
     */
    public function makePaymentOnOrder(Payment $payment, Order $order)
    {
        if (!is_null($order->getPaymentId()))
        {
            throw new \Exception('Order (' . $order->getOrderId() . ') has already been paid');
        }

        /** @var Payment $createdPayment */
        $createdPayment = $this->createPayment($payment);

        if (!is_null($createdPayment))
        {
            $order->setPaymentId($createdPayment->getPaymentId());
            $order->setDatePaymentReceived($this->getMySQLNow());
            $order->setStatus(OrderStatusType::PAYMENT_RECEIVED);

            $this->update($order, $order->getOrderId());
        }

        return $createdPayment;
    }

    public function createPayment(Payment $payment)
    {
        $payment->setConfirmationNumber($this->generateUniqueConfirmationNumber($payment));

        /** @var Payment $createdPayment */
        $createdPayment = $this->paymentDao->createRecord($payment);

        if (!is_null($createdPayment))
        {
            $this->getEventDispatcher()->dispatch(new PaymentCreatedEvent($createdPayment, null), 'Payment created successfully.');
        }

        return $createdPayment;
    }

    /**
     * @return mixed
     */
    public function getAllPayments()
    {
        return $this->paymentDao->getAll();
    }

    public function getPaymentById($paymentId)
    {
        return $this->paymentDao->getById($paymentId);
    }

    /**
     * @param Order $order
     * @return string
     */
    protected function generateUniqueOrderNumber(Order $order)
    {
        return md5($order->getAccountId() . $order->getType() . time() . uniqid());
    }

    /**
     * @param Payment $payment
     * @return string
     */
    protected function generateUniqueConfirmationNumber(Payment $payment)
    {
        return md5($payment->getPaymentTypeId() . $payment->getAmount() . $payment->getExternalConfirmationNumber() . time() . uniqid());
    }

}