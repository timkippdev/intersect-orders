<?php

namespace TimKipp\Intersect\Orders;

class CheckoutButtonScriptOptions {

    private $amount;
    private $class = 'stripe-button';
    private $description;
    private $email;
    private $label;
    private $requireZipCode = true;

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @param string $class
     */
    public function setClass(string $class)
    {
        $this->class = $class;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return bool
     */
    public function isRequireZipCode()
    {
        return $this->requireZipCode;
    }

    /**
     * @param bool $requireZipCode
     */
    public function setRequireZipCode(bool $requireZipCode)
    {
        $this->requireZipCode = $requireZipCode;
    }

}