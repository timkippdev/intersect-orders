<?php

namespace TimKipp\Intersect\Orders;

class OrderType {

    const ONLINE = 1;
    const INVOICE = 2;

}