<?php

namespace TimKipp\Intersect\Orders;

/**
 * Class OrderStatusType
 * @package TimKipp\Intersect\Order
 */
class OrderStatusType {

    const PENDING = 1;
    const PAYMENT_PENDING = 2;
    const PAYMENT_RECEIVED = 3;
    const PROCESSING = 4;
    const FULFILLED = 5;
    const CANCELLED = 6;

}