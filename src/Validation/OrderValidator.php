<?php

namespace TimKipp\Intersect\Orders\Validation;

use TimKipp\Intersect\Orders\OrderStatusType;
use TimKipp\Intersect\Orders\OrderType;
use TimKipp\Intersect\Validation\AbstractValidator;
use TimKipp\Intersect\Validation\ValidationException;

/**
 * Class OrderValidator
 * @package TimKipp\Intersect\Order\Validation
 */
class OrderValidator extends AbstractValidator {

    public function validateCreate($order)
    {
        /** @var Order $order */

        if ($order->getType() == OrderType::ONLINE)
        {
            if (is_null($order->getRawDetails()->getShippingAddress()))
            {
                throw new ValidationException('Shipping address is required for online orders');
            }
            if (is_null($order->getRawDetails()->getBillingAddress()))
            {
                throw new ValidationException('Billing address is required for online orders');
            }
        }
        else if ($order->getType() == OrderType::INVOICE)
        {
            if (is_null($order->getRawDetails()->getBillingAddress()))
            {
                throw new ValidationException('Billing address is required for invoice orders');
            }
        }
    }

    public function validateDelete($obj)
    {
        throw new ValidationException('Orders cannot be deleted');
    }

    public function validateUpdate($order)
    {
        /** @var Order $order */
    }

    public function validateCancel($order)
    {
        /** @var Order $order */
        if ($order->getStatus() == OrderStatusType::FULFILLED)
        {
            throw new ValidationException('Orders cannot be cancelled once fulfilled');
        }
    }

}