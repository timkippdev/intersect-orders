<?php

namespace TimKipp\Intersect\Orders\Validation;

use TimKipp\Intersect\Validation\AbstractValidator;
use TimKipp\Intersect\Validation\ValidationException;

class PaymentValidator extends AbstractValidator {

    public function validateCreate($payment)
    {
        /** @var Payment $payment */
        if (is_null($payment->getAmount()) || $payment->getAmount() <= 0.00)
        {
            throw new ValidationException('Payment amount must be greater than 0.00');
        }
    }

    public function validateDelete($payment)
    {
        throw new ValidationException('Payments cannot be deleted');
    }

    public function validateUpdate($payment)
    {
        /** @var Payment $payment */
    }

}