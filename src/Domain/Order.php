<?php

namespace TimKipp\Intersect\Orders\Domain;

use TimKipp\Intersect\Domain\AbstractTemporalDomain;

class Order extends AbstractTemporalDomain {

    public $orderId;
    public $accountId;
    public $orderNumber;
    public $type;
    /** @var OrderDetails $details */
    public $details;
    public $total;
    public $status;
    public $paymentId;
    public $dateCreated;
    public $dateUpdated;
    public $datePaymentReceived;
    public $dateFulfilled;
    public $dateCancelled;

    /** @var Payment $payment */
    public $payment;

    /** @var OrderDetails $rawDetails */
    private $rawDetails;

    /**
     * @return mixed|string
     */
    public function getTable()
    {
        return 'order';
    }

    public function getPrimaryKeyColumn()
    {
        return 'order_id';
    }

    public function getDateCreatedColumn()
    {
        return 'date_created';
    }

    public function getDateUpdatedColumn()
    {
        return 'date_updated';
    }

    public static function getColumnMappings()
    {
        return array(
            'order_id' => 'orderId',
            'account_id' => 'accountId',
            'order_number' => 'orderNumber',
            'type' => 'type',
            'details' => 'details',
            'total' => 'total',
            'status' => 'status',
            'payment_id' => 'paymentId',
            'date_created' => 'dateCreated',
            'date_updated' => 'dateUpdated',
            'date_payment_received' => 'datePaymentReceived',
            'date_fulfilled' => 'dateFulfilled',
            'date_cancelled' => 'dateCancelled'
        );
    }

    public static function getNullableColumns()
    {
        return array(
            'type',
            'payment_id',
            'date_payment_received',
            'date_fulfilled',
            'date_cancelled'
        );
    }

    /**
     * @return array
     */
    public static function getNonUpdatableColumns()
    {
        return array(
            'order_number',
            'type'
        );
    }

    /**
     * @return bool
     */
    public function isPaid()
    {
        return (!is_null($this->paymentId));
    }

    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * @param mixed $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @return mixed
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param mixed $accountId
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
    }

    /**
     * @return mixed
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param mixed $orderNumber
     */
    public function setOrderNumber($orderNumber)
    {
        $this->orderNumber = $orderNumber;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return OrderDetails
     */
    public function getDetails()
    {
        return unserialize($this->details);
    }

    /**
     * @param OrderDetails $details
     */
    public function setDetails(OrderDetails $details)
    {
        $this->rawDetails = $details;
        $this->details = serialize($details);
    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        if (is_null($this->total) && !is_null($this->getRawDetails()))
        {
            $this->total = $this->getRawDetails()->getTotal();
        }

        return $this->total;
    }

    /**
     * @param mixed $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * @param mixed $paymentId
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;
    }

    /**
     * @return Payment|null
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param Payment $payment
     */
    public function setPayment(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

    /**
     * @return mixed
     */
    public function getDatePaymentReceived()
    {
        return $this->datePaymentReceived;
    }

    /**
     * @param mixed $datePaymentReceived
     */
    public function setDatePaymentReceived($datePaymentReceived)
    {
        $this->datePaymentReceived = $datePaymentReceived;
    }

    /**
     * @return mixed
     */
    public function getDateFulfilled()
    {
        return $this->dateFulfilled;
    }

    /**
     * @param mixed $dateFulfilled
     */
    public function setDateFulfilled($dateFulfilled)
    {
        $this->dateFulfilled = $dateFulfilled;
    }

    /**
     * @return mixed
     */
    public function getDateCancelled()
    {
        return $this->dateCancelled;
    }

    /**
     * @param mixed $dateCancelled
     */
    public function setDateCancelled($dateCancelled)
    {
        $this->dateCancelled = $dateCancelled;
    }

    /**
     * @return OrderDetails
     */
    public function getRawDetails()
    {
        if (is_null($this->rawDetails) && !is_null($this->details))
        {
            $this->rawDetails = $this->getDetails();
        }
        
        return $this->rawDetails;
    }

}