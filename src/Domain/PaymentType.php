<?php

namespace TimKipp\Intersect\Orders\Domain;

use TimKipp\Intersect\Domain\AbstractTemporalDomain;

class PaymentType extends AbstractTemporalDomain {

    const CASH = 1;
    const STRIPE = 2;

    public $paymentTypeId;
    public $name;
    public $dateCreated;

    /**
     * @return mixed|string
     */
    public function getTable()
    {
        return 'order_payment_type';
    }

    public function getPrimaryKeyColumn()
    {
        return 'payment_type_id';
    }

    public static function getColumnMappings()
    {
        return array(
            'payment_type_id' => 'paymentTypeId',
            'name' => 'name',
            'date_created' => 'dateCreated'
        );
    }

    public function getDateCreatedColumn()
    {
        return 'date_created';
    }

    public function getDateUpdatedColumn()
    {
        return null;
    }

    /**
     * @return mixed
     */
    public function getPaymentTypeId()
    {
        return $this->paymentTypeId;
    }

    /**
     * @param mixed $paymentTypeId
     */
    public function setPaymentTypeId($paymentTypeId)
    {
        $this->paymentTypeId = $paymentTypeId;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

}