<?php

namespace TimKipp\Intersect\Orders\Domain;

use TimKipp\Intersect\Domain\AbstractTemporalDomain;

class Payment extends AbstractTemporalDomain {

    public $paymentId;
    public $paymentTypeId;
    public $amount;
    public $confirmationNumber;
    public $externalConfirmationNumber;
    public $dateCreated;

    /**
     * @return mixed|string
     */
    public function getTable()
    {
        return 'order_payment';
    }

    public function getPrimaryKeyColumn()
    {
        return 'payment_id';
    }

    public static function getColumnMappings()
    {
        return array(
            'payment_id' => 'paymentId',
            'payment_type_id' => 'paymentTypeId',
            'amount' => 'amount',
            'confirmation_number' => 'confirmationNumber',
            'external_confirmation_number' => 'externalConfirmationNumber',
            'date_created' => 'dateCreated'
        );
    }

    public static function getNullableColumns()
    {
        return array(
            'external_confirmation_number'
        );
    }

    public static function getNonUpdatableColumns()
    {
        return array(
            'payment',
            'amount',
            'confirmation_number'
        );
    }

    public function getDateCreatedColumn()
    {
        return 'date_created';
    }

    public function getDateUpdatedColumn()
    {
        return null;
    }

    /**
     * @return mixed
     */
    public function getPaymentId()
    {
        return $this->paymentId;
    }

    /**
     * @param mixed $paymentId
     */
    public function setPaymentId($paymentId)
    {
        $this->paymentId = $paymentId;
    }

    /**
     * @return mixed
     */
    public function getPaymentTypeId()
    {
        return $this->paymentTypeId;
    }

    /**
     * @param mixed $paymentTypeId
     */
    public function setPaymentTypeId($paymentTypeId)
    {
        $this->paymentTypeId = $paymentTypeId;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return mixed
     */
    public function getConfirmationNumber()
    {
        return $this->confirmationNumber;
    }

    /**
     * @param mixed $confirmationNumber
     */
    public function setConfirmationNumber($confirmationNumber)
    {
        $this->confirmationNumber = $confirmationNumber;
    }

    /**
     * @return mixed
     */
    public function getExternalConfirmationNumber()
    {
        return $this->externalConfirmationNumber;
    }

    /**
     * @param mixed $externalConfirmationNumber
     */
    public function setExternalConfirmationNumber($externalConfirmationNumber)
    {
        $this->externalConfirmationNumber = $externalConfirmationNumber;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

}