<?php

namespace TimKipp\Intersect\Tests\Unit\Order;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Orders\Domain\OrderDetails;
use TimKipp\Intersect\Orders\Domain\ShippingAddress;
use TimKipp\Intersect\Orders\OrderStatusType;
use TimKipp\Intersect\Orders\OrderType;
use TimKipp\Intersect\Orders\Validation\OrderValidator;
use TimKipp\Intersect\Tests\Stubs\TestOrder;

class OrderValidatorTest extends TestCase {

    /** @var OrderValidator $orderValidator */
    private $orderValidator;

    protected function setUp()
    {
        parent::setUp();

        $this->orderValidator = new OrderValidator();
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Shipping address is required for online orders
     */
    public function test_validateCreate_onlineShippingRequired()
    {
        $orderDetails = new OrderDetails();

        $order = new TestOrder(1);
        $order->setType(OrderType::ONLINE);
        $order->setDetails($orderDetails);

        $this->orderValidator->validateCreate($order);
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Billing address is required for online orders
     */
    public function test_validateCreate_onlineBillingRequired()
    {
        $orderDetails = new OrderDetails();
        $orderDetails->setShippingAddress(new ShippingAddress());

        $order = new TestOrder(1);
        $order->setType(OrderType::ONLINE);
        $order->setDetails($orderDetails);

        $this->orderValidator->validateCreate($order);
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Billing address is required for invoice orders
     */
    public function test_validateCreate_invoiceBillingRequired()
    {
        $orderDetails = new OrderDetails();
        $orderDetails->setShippingAddress(new ShippingAddress());

        $order = new TestOrder(1);
        $order->setType(OrderType::INVOICE);
        $order->setDetails($orderDetails);

        $this->orderValidator->validateCreate($order);
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Orders cannot be deleted
     */
    public function test_validateDelete()
    {
        $order = new TestOrder(1);
        $this->orderValidator->validateDelete($order);
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Orders cannot be cancelled once fulfilled
     */
    public function test_validateCancel_fulfilledOrdersCannotBeCancelled()
    {
        $order = new TestOrder(1);
        $order->setStatus(OrderStatusType::FULFILLED);

        $this->orderValidator->validateCancel($order);
    }

}