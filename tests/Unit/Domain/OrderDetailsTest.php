<?php

namespace TimKipp\Intersect\Tests\Unit\Domain;

use PHPUnit\Framework\TestCase;
use TimKipp\Intersect\Orders\Domain\OrderDetails;

class OrderDetailsTest extends TestCase {

    public function test_getTotal_noDiscount()
    {
        $orderDetails = new OrderDetails();
        $orderDetails->setSubtotal(5.00);
        $orderDetails->setTax(2.50);
        $orderDetails->setShipping(2.50);

        $this->assertEquals(10.00, $orderDetails->getTotal());
    }

    public function test_getTotal_withDiscount()
    {
        // discount should only be applied against the subtotal and the subtotal should never be less than zero

        $orderDetails = new OrderDetails();
        $orderDetails->setSubtotal(5.00);
        $orderDetails->setTax(2.50);
        $orderDetails->setShipping(2.50);
        $orderDetails->setDiscount(7.50);

        $this->assertEquals(5.00, $orderDetails->getTotal());
    }

}