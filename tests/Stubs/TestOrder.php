<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Orders\Domain\Order;
use TimKipp\Intersect\Orders\OrderStatusType;

class TestOrder extends Order {

    public function __construct($accountId)
    {
        $suffix = uniqid();

        $orderDetails = new TestOrderDetails();

        $this->setAccountId($accountId);
        $this->setOrderNumber(sha1(time() . $suffix));
        $this->setDetails($orderDetails);
        $this->setTotal('1.23');
        $this->setType('Test Order');
        $this->setStatus(OrderStatusType::PENDING);
    }

}