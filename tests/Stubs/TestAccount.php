<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Types\AccountStatusType;

class TestAccount extends Account {

    public function __construct()
    {
        $suffix = uniqid();

        $this->setAccountId(1);
        $this->setEmail('email-' . $suffix . '@test.com');
        $this->setUsername('username-' . $suffix);
        $this->setPassword('password-' . $suffix);
        $this->setAutoLoginToken('auto-login-token-' . $suffix);
        $this->setStatus(AccountStatusType::PENDING);
        $this->setFirstName('firstName');
        $this->setLastName('lastName');
        $this->setVerificationCode('verificationCode-' . $suffix);
    }

}