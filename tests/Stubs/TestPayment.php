<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Orders\Domain\Payment;
use TimKipp\Intersect\Orders\Domain\PaymentType;

class TestPayment extends Payment {

    public function __construct()
    {
        $confirmationNumber= sha1(time() . uniqid());

        $this->setAmount(12.34);
        $this->setConfirmationNumber($confirmationNumber);
        $this->setExternalConfirmationNumber('ext_' . $confirmationNumber);
        $this->setPaymentTypeId(PaymentType::CASH);
    }

}