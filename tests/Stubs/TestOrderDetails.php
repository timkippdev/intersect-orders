<?php

namespace TimKipp\Intersect\Tests\Stubs;

use TimKipp\Intersect\Orders\Domain\OrderDetails;

class TestOrderDetails extends OrderDetails {

    public function __construct()
    {
        $this->setSubtotal(20.00);
        $this->setShipping(5.00);
        $this->setTax(5.00);
        $this->setDiscount(10.00);
    }

}