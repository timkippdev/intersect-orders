<?php

namespace TimKipp\Intersect\Tests\IT\Services;

use TimKipp\Intersect\Tests\IT\BaseITTest;
use TimKipp\Intersect\Tests\Stubs\TestPaymentType;

class PaymentTypeServiceTest extends BaseITTest {

    public function test_create()
    {
        $createdPaymentType = $this->paymentTypeService->create(new TestPaymentType());

        $this->assertNotNull($createdPaymentType);
    }

    public function test_delete()
    {
        $createdPaymentType = $this->paymentTypeService->create(new TestPaymentType());
        $this->assertNotNull($createdPaymentType);

        $this->paymentTypeService->delete($createdPaymentType);

        $this->assertNull($this->paymentTypeService->getById($createdPaymentType->getPaymentTypeId()));
    }

    public function test_update()
    {
        $createdPaymentType = $this->paymentTypeService->create(new TestPaymentType());
        $this->assertNotNull($createdPaymentType);

        $updatedPaymentTypeName = 'updated_payment_type_' . uniqid();
        $createdPaymentType->setName($updatedPaymentTypeName);

        $updatedPaymentType = $this->paymentTypeService->update($createdPaymentType, $createdPaymentType->getPaymentTypeId());

        $this->assertNotNull($updatedPaymentType);
        $this->assertEquals($updatedPaymentTypeName, $updatedPaymentType->getName());
    }

}