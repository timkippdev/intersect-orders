<?php

namespace TimKipp\Intersect\Tests\IT\Services;

use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Event\Event;
use TimKipp\Intersect\Orders\Event\OrderCancelledEvent;
use TimKipp\Intersect\Orders\Event\OrderCreatedEvent;
use TimKipp\Intersect\Orders\Event\PaymentCreatedEvent;
use TimKipp\Intersect\Orders\OrderStatusType;
use TimKipp\Intersect\Tests\IT\BaseITTest;
use TimKipp\Intersect\Tests\Stubs\TestAccount;
use TimKipp\Intersect\Tests\Stubs\TestListener;
use TimKipp\Intersect\Tests\Stubs\TestOrder;
use TimKipp\Intersect\Tests\Stubs\TestPayment;

class OrderServiceTest extends BaseITTest {

    /** @var Account $ACCOUNT */
    private static $ACCOUNT;

    protected function setUp()
    {
        parent::setUp();

        if (is_null(self::$ACCOUNT))
        {
            self::$ACCOUNT = $this->accountService->create(new TestAccount());
        }
    }

    public function test_create_success()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());

        /** @var TestOrder $orderCreated */
        $orderCreated = $this->orderService->create($order);

        $this->assertNotNull($orderCreated);
        $this->assertNotNull($orderCreated->getOrderId());
        $this->assertNotNull($orderCreated->getDateCreated());
    }

    public function test_create_guestOrder()
    {
        $order = new TestOrder(null);

        /** @var TestOrder $orderCreated */
        $orderCreated = $this->orderService->create($order);

        $this->assertNotNull($orderCreated);
        $this->assertNotNull($orderCreated->getOrderId());
        $this->assertNotNull($orderCreated->getDateCreated());
    }

    public function test_create_statusMustBePending()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());
        $order->setStatus(OrderStatusType::PROCESSING);

        /** @var TestOrder $orderCreated */
        $orderCreated = $this->orderService->create($order);

        $this->assertNotNull($orderCreated);
        $this->assertEquals(OrderStatusType::PENDING, $orderCreated->getStatus());
    }

    public function test_create_totalBasedOffDetails()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());
        $orderDetails = $order->getRawDetails();

        /** @var TestOrder $orderCreated */
        $orderCreated = $this->orderService->create($order);

        $this->assertNotNull($orderCreated);
        $this->assertEquals($orderDetails->getTotal(), $orderCreated->getTotal());
    }

    public function test_create_eventDispatched()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());
        $testListener = new TestListener();

        $this->orderService->getEventDispatcher()->addListener(OrderCreatedEvent::ORDER_CREATED, $testListener);

        /** @var TestOrder $orderCreated */
        $orderCreated = $this->orderService->create($order);
        $this->assertNotNull($orderCreated);

        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Order created successfully.', $testListener->getReceivedMessage());
    }

    public function test_createWithPayment()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());

        /** @var TestOrder $orderCreated */
        $payment = new TestPayment();
        $orderCreated = $this->orderService->createWithPayment($order, $payment);

        $this->assertNotNull($orderCreated);
        $this->assertNotNull($orderCreated->getOrderId());
        $this->assertNotNull($orderCreated->getPaymentId());
        $this->assertNotNull($orderCreated->getPayment());
        $this->assertEquals(OrderStatusType::PAYMENT_RECEIVED, $orderCreated->getStatus());
    }

    public function test_updateOrder_orderNumberCannotBeUpdated()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());

        /** @var TestOrder $orderCreated */
        $orderCreated = $this->orderService->create($order);

        $this->assertNotNull($orderCreated);
        $this->assertNotNull($orderCreated->getOrderId());

        $originalOrderNumber = $orderCreated->getOrderNumber();

        $this->assertNotNull($originalOrderNumber);

        $orderCreated->setOrderNumber('thisshouldnotbeupdated');

        $updatedOrder = $this->orderService->update($orderCreated, $orderCreated->getOrderId());

        $this->assertNotNull($updatedOrder);
        $this->assertEquals($originalOrderNumber, $updatedOrder->getOrderNumber());
        $this->assertNotNull($updatedOrder->getDateUpdated());
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Orders cannot be deleted
     */
    public function test_deleteOrder()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());

        /** @var TestOrder $orderCreated */
        $orderCreated = $this->orderService->create($order);

        $this->assertNotNull($orderCreated);
        $this->assertNotNull($orderCreated->getOrderId());

        $this->orderService->delete($orderCreated);
    }

    public function test_getOrdersForAccountId()
    {
        $account = $this->accountService->create(new TestAccount());
        $this->orderService->create(new TestOrder($account->getAccountId()));
        $this->orderService->create(new TestOrder($account->getAccountId()));
        $this->orderService->create(new TestOrder($account->getAccountId()));

        $orders = $this->orderService->getOrdersForAccountId($account->getAccountId());

        $this->assertNotNull($orders);
        $this->assertCount(3, $orders);
    }

    public function test_getOrdersForAccountId_includePayments()
    {
        $account = $this->accountService->create(new TestAccount());

        $orderCreated = $this->orderService->create(new TestOrder($account->getAccountId()));
        $payment = $this->orderService->makePaymentOnOrder(new TestPayment(), $orderCreated);

        $orders = $this->orderService->getOrdersForAccountId($account->getAccountId(), true);

        $this->assertNotNull($orders);
        $this->assertCount(1, $orders);

        /** @var Order $order */
        foreach ($orders as $order)
        {
            $this->assertNotNull($order->getPaymentId());
            $this->assertNotNull($order->getPayment());
            $this->assertTrue($order->isPaid());

            $this->assertEquals($payment->getPaymentId(), $order->getPaymentId());
        }
    }

    public function test_cancelOrder_verifyStatus()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());

        $orderCreated = $this->orderService->create($order);
        $this->assertNotNull($orderCreated);

        /** @var TestOrder $cancelledOrder */
        $cancelledOrder = $this->orderService->cancelOrder($orderCreated);

        $this->assertEquals(OrderStatusType::CANCELLED, $cancelledOrder->getStatus());
        $this->assertNotNull($cancelledOrder->getDateCancelled());
    }

    public function test_cancelOrder_eventDispatched()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());
        $testListener = new TestListener();

        $this->orderService->getEventDispatcher()->addListener(OrderCancelledEvent::ORDER_CANCELLED, $testListener);

        $orderCreated = $this->orderService->create($order);
        $this->assertNotNull($orderCreated);

        /** @var TestOrder $cancelledOrder */
        $this->orderService->cancelOrder($orderCreated);

        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Order cancelled successfully.', $testListener->getReceivedMessage());
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Orders cannot be cancelled once fulfilled
     */
    public function test_cancelOrder_cannotCancelIfFulfilled()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());

        /** @var TestOrder $orderCreated */
        $orderCreated = $this->orderService->create($order);
        $this->assertNotNull($orderCreated);

        $orderCreated->setStatus(OrderStatusType::FULFILLED);

        /** @var TestOrder $updatedOrder */
        $updatedOrder = $this->orderService->update($orderCreated, $orderCreated->getOrderId());
        $this->assertEquals(OrderStatusType::FULFILLED, $updatedOrder->getStatus());

        $this->orderService->cancelOrder($orderCreated);
    }

    public function test_makePaymentOnOrder()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());

        /** @var TestOrder $orderCreated */
        $orderCreated = $this->orderService->create($order);
        /** @var Payment $paymentCreated */
        $paymentCreated = $this->orderService->makePaymentOnOrder(new TestPayment(), $orderCreated);

        $this->assertNotNull($orderCreated);
        $this->assertNotNull($paymentCreated);

        /** @var TestOrder $updatedOrder */
        $updatedOrder = $this->orderService->getById($orderCreated->getOrderId());

        $this->assertNotNull($updatedOrder);
        $this->assertEquals($paymentCreated->getPaymentId(), $updatedOrder->getPaymentId());
        $this->assertEquals(OrderStatusType::PAYMENT_RECEIVED, $updatedOrder->getStatus());
        $this->assertNotNull($updatedOrder->getDatePaymentReceived());
    }

    public function test_makePaymentOnOrder_eventDispatched()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());
        $testListener = new TestListener();

        $this->orderService->getEventDispatcher()->addListener(PaymentCreatedEvent::PAYMENT_CREATED, $testListener);

        /** @var TestOrder $orderCreated */
        $orderCreated = $this->orderService->create($order);
        /** @var Payment $paymentCreated */
        $paymentCreated = $this->orderService->makePaymentOnOrder(new TestPayment(), $orderCreated);

        $this->assertNotNull($orderCreated);
        $this->assertNotNull($paymentCreated);
        $this->assertTrue($testListener->getTriggered());
        $this->assertEquals('Payment created successfully.', $testListener->getReceivedMessage());
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Payment amount must be greater than 0.00
     */
    public function test_makePaymentOnOrder_negativeAmount()
    {
        $payment = new TestPayment();
        $payment->setAmount(-1.12);

        $this->orderService->makePaymentOnOrder($payment, new TestOrder(self::$ACCOUNT->getAccountId()));
    }

    public function test_getOrderByOrderNumber()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());
        $createdOrder = $this->orderService->create($order);

        $this->assertNotNull($createdOrder);

        $orderByOrderNumber = $this->orderService->getOrderByOrderNumber($createdOrder->getOrderNumber());

        $this->assertNotNull($orderByOrderNumber);
        $this->assertEquals($createdOrder->getOrderId(), $orderByOrderNumber->getOrderId());
    }
    
}