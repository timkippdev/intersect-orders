<?php

define('DB_HOST', '127.0.0.1');
define('DB_USER', 'root');
define('DB_PASS', 'root');
define('DB_NAME', 'intersect_integration_tests');
define('DB_PORT', '3306');
define('DB_DELETE', true);

define('GENERIC_TABLE_NAME', 'generic_test');