<?php

namespace TimKipp\Intersect\Tests\IT;

use TimKipp\Intersect\Database\Adapters\MySQLAdapter;
use TimKipp\Intersect\Utility\DatabaseUtility;

require_once 'test-config.php';

class TestDatabaseAdapter extends MySQLAdapter {

    public function __construct()
    {
        parent::__construct(DB_HOST, DB_USER, DB_PASS);
    }

    public function getDbName()
    {
        return DB_NAME;
    }

    /**
     * @deprecated
     *
     * @param $sql
     * @param array $parameters
     * @return bool|\TimKipp\Intersect\Database\DatabaseResult
     */
    public function query($sql, $parameters = array())
    {
        $result = parent::query($sql, $parameters);

        if (isset($this->getConnection()->errorInfo()[2]))
        {
            echo 'QUERY FAILED: <br />' . $this->getConnection()->errorInfo()[2] . PHP_EOL;
            echo '      ' . DatabaseUtility::removeQueryLineBreaks($sql) . PHP_EOL;
            return false;
        }

        return $result;
    }

}