<?php

namespace TimKipp\Intersect\Tests\IT\Dao;

use TimKipp\Intersect\Database\Dao\AccountDao;
use TimKipp\Intersect\Orders\Dao\OrderDao;
use TimKipp\Intersect\Orders\Dao\PaymentDao;
use TimKipp\Intersect\Orders\Dao\PaymentTypeDao;
use TimKipp\Intersect\Tests\IT\BaseITTest;

abstract class BaseDaoTest extends BaseITTest {

    /** @var AccountDao $accountDao */
    protected $accountDao;

    /** @var OrderDao $orderDao */
    protected $orderDao;

    /** @var PaymentDao $paymentDao */
    protected $paymentDao;

    /** @var PaymentTypeDao $paymentTypeDao */
    protected $paymentTypeDao;

    protected function setUp()
    {
        parent::setUp();

        $this->accountDao = new AccountDao(self::$DB);
        $this->orderDao = new OrderDao(self::$DB);
        $this->paymentDao = new PaymentDao(self::$DB);
        $this->paymentTypeDao = new PaymentTypeDao(self::$DB);
    }

}