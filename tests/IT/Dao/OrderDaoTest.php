<?php

namespace TimKipp\Intersect\Tests\IT\Dao;

use TimKipp\Intersect\Domain\Account;
use TimKipp\Intersect\Tests\Stubs\TestAccount;
use TimKipp\Intersect\Tests\Stubs\TestOrder;

class OrderDaoTest extends BaseDaoTest {

    /** @var Account $ACCOUNT */
    private static $ACCOUNT;

    protected function setUp()
    {
        parent::setUp();

        if (is_null(self::$ACCOUNT))
        {
            self::$ACCOUNT = $this->accountDao->createRecord(new TestAccount());
        }
    }

    public function test_createOrder_success()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());

        /** @var TestOrder $orderCreated */
        $orderCreated = $this->orderDao->createRecord($order);

        $this->assertNotNull($orderCreated);
        $this->assertNotNull($orderCreated->getOrderId());
        $this->assertNotNull($orderCreated->getDateCreated());
    }

    public function test_updateOrder_success()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());

        /** @var TestOrder $orderCreated */
        $orderCreated = $this->orderDao->createRecord($order);

        $this->assertNotNull($orderCreated);
        $this->assertNotNull($orderCreated->getOrderId());

        $updatedOrderNumber = sha1('updated'. time());

        //$orderCreated->setOrderNumber($updatedOrderNumber);
        $orderCreated->setDateFulfilled($this->getMySQLNow());

        $updatedOrder = $this->orderDao->updateRecord($orderCreated, $orderCreated->getOrderId());

        $this->assertNotNull($updatedOrder);
        $this->assertEquals($orderCreated->getDateFulfilled(), $updatedOrder->getDateFulfilled());
        $this->assertNotNull($updatedOrder->getDateUpdated());
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Orders cannot be deleted
     */
    public function test_deleteOrder()
    {
        $order = new TestOrder(self::$ACCOUNT->getAccountId());

        /** @var TestOrder $orderCreated */
        $orderCreated = $this->orderDao->createRecord($order);

        $this->assertNotNull($orderCreated);
        $this->assertNotNull($orderCreated->getOrderId());

        $this->orderDao->deleteRecord($orderCreated);
    }

    public function test_getOrdersForAccountId()
    {
        $account = $this->accountService->create(new TestAccount());
        $this->orderDao->createRecord(new TestOrder($account->getAccountId()));
        $this->orderDao->createRecord(new TestOrder($account->getAccountId()));
        $this->orderDao->createRecord(new TestOrder($account->getAccountId()));

        $orders = $this->orderDao->getOrdersForAccountId($account->getAccountId());

        $this->assertNotNull($orders);
        $this->assertCount(3, $orders);
    }

}