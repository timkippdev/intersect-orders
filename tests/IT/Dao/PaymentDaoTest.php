<?php

namespace TimKipp\Intersect\Tests\IT\Dao;

use TimKipp\Intersect\Orders\Domain\Payment;
use TimKipp\Intersect\Tests\Stubs\TestPayment;

class PaymentDaoTest extends BaseDaoTest {

    public function test_createPayment_success()
    {
        $payment = new TestPayment();

        /** @var TestPayment $paymentCreated */
        $paymentCreated = $this->paymentDao->createRecord($payment);

        $this->assertNotNull($paymentCreated);
        $this->assertNotNull($paymentCreated->getPaymentId());
        $this->assertNotNull($paymentCreated->getDateCreated());
    }

    public function test_updatePayment_success()
    {
        $payment = new TestPayment();

        /** @var TestPayment $paymentCreated */
        $paymentCreated = $this->paymentDao->createRecord($payment);

        $this->assertNotNull($paymentCreated);
        $this->assertNotNull($paymentCreated->getPaymentId());

        $updatedExternalConfirmationNumber = sha1('updated'. time());

        $paymentCreated->setExternalConfirmationNumber($updatedExternalConfirmationNumber);

        /** @var Payment $updatedOrder */
        $updatedOrder = $this->paymentDao->updateRecord($paymentCreated, $paymentCreated->getPaymentId());

        $this->assertNotNull($updatedOrder);
        $this->assertEquals($updatedExternalConfirmationNumber, $updatedOrder->getExternalConfirmationNumber());
    }

    /**
     * @expectedException \TimKipp\Intersect\Validation\ValidationException
     * @expectedExceptionMessage Payments cannot be deleted
     */
    public function test_deletePayment()
    {
        $payment = new TestPayment();

        /** @var TestPayment $paymentCreated */
        $paymentCreated = $this->paymentDao->createRecord($payment);

        $this->assertNotNull($paymentCreated);
        $this->assertNotNull($paymentCreated->getPaymentId());

        $this->paymentDao->deleteRecord($paymentCreated);

        $this->assertNull($this->paymentDao->getById($paymentCreated->getPaymentId()));
    }

}