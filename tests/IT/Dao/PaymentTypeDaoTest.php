<?php

namespace TimKipp\Intersect\Tests\IT\Dao;

use TimKipp\Intersect\Tests\Stubs\TestPaymentType;

class PaymentTypeDaoTest extends BaseDaoTest {

    public function test_create()
    {
        $createdPaymentType = $this->paymentTypeDao->createRecord(new TestPaymentType());

        $this->assertNotNull($createdPaymentType);
    }

    public function test_delete()
    {
        $createdPaymentType = $this->paymentTypeDao->createRecord(new TestPaymentType());
        $this->assertNotNull($createdPaymentType);

        $this->paymentTypeDao->deleteRecord($createdPaymentType);

        $this->assertNull($this->paymentTypeDao->getById($createdPaymentType->getPaymentTypeId()));
    }

    public function test_update()
    {
        $createdPaymentType = $this->paymentTypeDao->createRecord(new TestPaymentType());
        $this->assertNotNull($createdPaymentType);

        $updatedPaymentTypeName = 'updated_payment_type_' . uniqid();
        $createdPaymentType->setName($updatedPaymentTypeName);

        $updatedPaymentType = $this->paymentTypeDao->updateRecord($createdPaymentType, $createdPaymentType->getPaymentTypeId());

        $this->assertNotNull($updatedPaymentType);
        $this->assertEquals($updatedPaymentTypeName, $updatedPaymentType->getName());
    }

}