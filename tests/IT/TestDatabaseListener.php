<?php

namespace TimKipp\Intersect\Tests\IT;

use PHPUnit\Framework\BaseTestListener;
use PHPUnit\Framework\TestSuite;
use TimKipp\Intersect\Migration\MigrationRunner;

class TestDatabaseListener extends BaseTestListener {

    private $integrationTestsSuiteName = 'intersect-integration-tests';

    /** @var TestDatabaseAdapter $databaseAdapter */
    private $databaseAdapter;
    private $databaseName;

    public function startTestSuite(TestSuite $suite)
    {
        parent::startTestSuite($suite);

        if ($suite->getName() == $this->integrationTestsSuiteName)
        {
            $this->databaseAdapter = new TestDatabaseAdapter();
            $this->databaseName = $this->databaseAdapter->getDbName();

            $this->databaseAdapter->createDatabase($this->databaseName);
            $this->databaseAdapter->selectDatabase($this->databaseName);

            $coreMigrationRunner = new MigrationRunner($this->databaseAdapter, dirname(dirname(dirname(__FILE__))) . '/vendor/timkipp/intersect/src/Migration/Migration');
            $coreMigrationRunner->upgradeToLatest();

            $coreMigrationRunner = new MigrationRunner($this->databaseAdapter, dirname(dirname(dirname(__FILE__))) . '/src/Migrations');
            $coreMigrationRunner->upgradeToLatest();

            $this->generateGenericTestTable();

            echo 'Starting Tests...' . PHP_EOL . PHP_EOL;
        }
    }

    public function endTestSuite(TestSuite $suite)
    {
        parent::endTestSuite($suite);

        if ($suite->getName() == $this->integrationTestsSuiteName)
        {
            echo PHP_EOL . 'Tests Complete!' . PHP_EOL . PHP_EOL;

            if (DB_DELETE)
            {
                $this->databaseAdapter->dropDatabase($this->databaseName);
            }
        }
    }

    private function generateGenericTestTable()
    {
        $this->databaseAdapter->query("
            CREATE TABLE IF NOT EXISTS `" . GENERIC_TABLE_NAME . "` (
              `id` INT(11) NOT NULL AUTO_INCREMENT,
              `message` VARCHAR(100) NOT NULL,
              PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8;
        ");
    }
}